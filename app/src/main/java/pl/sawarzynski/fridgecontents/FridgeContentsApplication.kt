package pl.sawarzynski.fridgecontents

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import pl.sawarzynski.fridgecontents.data.AppDatabase
import pl.sawarzynski.fridgecontents.data.contents.ProductRepository
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPositionRepository

class FridgeContentsApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { AppDatabase.getDatabase(this, applicationScope) }
    val productRepository by lazy { ProductRepository(database.productDao()) }
    val wishListPositionRepository by lazy { WishListPositionRepository(database.wishListPositionDao()) }
}
