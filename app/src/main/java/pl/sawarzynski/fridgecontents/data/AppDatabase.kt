package pl.sawarzynski.fridgecontents.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import kotlinx.coroutines.CoroutineScope
import pl.sawarzynski.fridgecontents.data.contents.Product
import pl.sawarzynski.fridgecontents.data.contents.ProductDao
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPosition
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPositionDao


@Database(
    entities = [Product::class, WishListPosition::class],
    version = 3
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun productDao(): ProductDao
    abstract fun wishListPositionDao(): WishListPositionDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "fridge_contents"
                )
                .build()
                INSTANCE = instance

                instance
            }
        }
    }
}
