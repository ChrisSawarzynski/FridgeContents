package pl.sawarzynski.fridgecontents.data.wishlist

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface WishListPositionDao {
    @Query("SELECT * FROM wishlistposition ORDER BY name, id")
    fun getAll(): Flow<List<WishListPosition>>

    @Query("SELECT * FROM wishlistposition WHERE taken = 0 ORDER BY name, id")
    fun getNotTaken(): Flow<List<WishListPosition>>

    @Query("SELECT * FROM wishlistposition WHERE id IN (:positionsIds) ORDER BY name, id")
    fun loadAllByIds(positionsIds: IntArray): Flow<List<WishListPosition>>

    @Query("SELECT * FROM wishlistposition WHERE name LIKE :name LIMIT 1")
    fun findByName(name: String): Flow<WishListPosition>

    @Insert
    suspend fun insertAll(vararg products: WishListPosition)

    @Update
    suspend fun update(product: WishListPosition)

    @Delete
    suspend fun delete(product: WishListPosition)

    @Query("DELETE FROM wishlistposition")
    suspend fun deleteAll()
}