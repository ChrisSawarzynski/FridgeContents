package pl.sawarzynski.fridgecontents.data.contents

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class ProductRepository(private val productDao: ProductDao) {
    val products: Flow<List<Product>> = productDao.getAll()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(product: Product) {
        productDao.insertAll(product)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(product: Product) {
        productDao.update(product)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun delete(product: Product) {
        productDao.delete(product)
    }
}