package pl.sawarzynski.fridgecontents.data.wishlist

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class WishListPositionRepository(private val wishListPositionDao: WishListPositionDao) {
    val positions: Flow<List<WishListPosition>> = wishListPositionDao.getAll()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(positions: WishListPosition) {
        wishListPositionDao.insertAll(positions)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(position: WishListPosition) {
        wishListPositionDao.update(position)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun delete(position: WishListPosition) {
        wishListPositionDao.delete(position)
    }
}