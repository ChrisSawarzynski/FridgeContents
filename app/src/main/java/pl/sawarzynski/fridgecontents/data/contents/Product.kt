package pl.sawarzynski.fridgecontents.data.contents

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDate
import java.time.LocalDateTime

@Entity
data class Product(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "added") val added: LocalDateTime,
    @ColumnInfo(name = "expires") val expires: LocalDate,
)
