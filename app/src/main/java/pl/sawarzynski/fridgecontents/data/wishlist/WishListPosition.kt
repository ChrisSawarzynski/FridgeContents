package pl.sawarzynski.fridgecontents.data.wishlist

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDateTime

@Entity
data class WishListPosition(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "added") val added: LocalDateTime,
    @ColumnInfo(name = "number") val number: Int,
    @ColumnInfo(name = "taken") var taken: Boolean,
)
