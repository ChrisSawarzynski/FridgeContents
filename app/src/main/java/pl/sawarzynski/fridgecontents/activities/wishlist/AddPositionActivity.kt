package pl.sawarzynski.fridgecontents.activities.wishlist

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.firebase.ui.auth.AuthUI
import com.google.android.material.snackbar.Snackbar
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import kotlinx.android.synthetic.main.wishlist_add_position_activity.*
import pl.sawarzynski.fridgecontents.FridgeContentsApplication
import pl.sawarzynski.fridgecontents.R
import pl.sawarzynski.fridgecontents.activities.MainActivity
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPosition
import pl.sawarzynski.fridgecontents.model.wishlist.PositionViewModel
import pl.sawarzynski.fridgecontents.model.wishlist.PositionViewModelFactory
import java.time.LocalDateTime
import java.util.*


class AddPositionActivity : AppCompatActivity() {
    private val positionViewModel: PositionViewModel by viewModels {
        PositionViewModelFactory(
            (application as FridgeContentsApplication).wishListPositionRepository,
            (application as FridgeContentsApplication).productRepository,
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wishlist_add_position_activity)

        val toolbar = appToolbar as Toolbar
        toolbar.title = getString(R.string.add_to_wishlist_title)
        setSupportActionBar(toolbar)

        editNumberInput.setText("1")

        addProductButton.setOnClickListener {
            val replyIntent = Intent()
            replyIntent.putExtra(POSITION_NAME, editNameInput.text.toString())
            replyIntent.putExtra(POSITION_NUMBER, editNumberInput.text.toString().toInt())
            setResult(Activity.RESULT_OK, replyIntent)
            finish()
        }

        readFromCamera.setOnClickListener {
            addProductButton.setOnClickListener {  }

            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAPTURE_IMAGE_REQUEST)
        }
    }

    companion object {
        const val POSITION_NAME = "POSITION_NAME"
        const val POSITION_NUMBER = "POSITION_NUMBER"

        const val CAPTURE_IMAGE_REQUEST = 2137
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CAPTURE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            val bitmap = data?.extras?.get("data") as Bitmap
            val image = InputImage.fromBitmap(bitmap, 0)

            val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
            recognizer.process(image)
                .addOnSuccessListener { visionText ->
                    for (block in visionText.textBlocks) {
                        val position = WishListPosition(
                            UUID.randomUUID().toString(),
                            block.text.lowercase(),
                            LocalDateTime.now(),
                            1,
                            false
                        )

                        positionViewModel.insert(position)
                    }

                    Snackbar.make(
                        readFromCamera,
                        getString(R.string.camera_read_successfull),
                        Snackbar.LENGTH_SHORT
                    ).show()

                    Handler().postDelayed(Runnable {
                        val replyIntent = Intent()
                        setResult(Activity.RESULT_CANCELED, replyIntent)
                        finish()
                    }, 2000)
                }
                .addOnFailureListener { _ ->
                    Snackbar.make(
                        readFromCamera,
                        getString(R.string.camera_Read_impossible),
                        Snackbar.LENGTH_SHORT
                    ).show()

                    addProductButton.setOnClickListener {
                        val replyIntent = Intent()
                        replyIntent.putExtra(POSITION_NAME, editNameInput.text.toString())
                        replyIntent.putExtra(POSITION_NUMBER, editNumberInput.text.toString().toInt())
                        setResult(Activity.RESULT_OK, replyIntent)
                        finish()
                    }
                }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.wishlist_add_position_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_back) {
            finish()
            return true
        }

        if (item.itemId == R.id.action_sign_out) {
            AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    finish()
                    startActivity(intent)
                }
        }

        return super.onOptionsItemSelected(item)
    }
}