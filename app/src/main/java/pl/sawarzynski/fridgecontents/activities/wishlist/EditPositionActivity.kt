package pl.sawarzynski.fridgecontents.activities.wishlist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.firebase.ui.auth.AuthUI
import kotlinx.android.synthetic.main.wishlist_edit_position_activity.*
import pl.sawarzynski.fridgecontents.R
import pl.sawarzynski.fridgecontents.activities.MainActivity

class EditPositionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wishlist_edit_position_activity)

        val toolbar = appToolbar as Toolbar
        toolbar.title = getString(R.string.wishlist_title)
        setSupportActionBar(toolbar)

        editNameInput.setText(intent.getStringExtra(POSITION_NAME))
        editNumberInput.setText(intent.getIntExtra(POSITION_NUMBER, 1).toString())

        editPositionButton.setOnClickListener {
            val replyIntent = Intent()
            replyIntent.putExtra(POSITION_ID, intent.getStringExtra(POSITION_ID))
            replyIntent.putExtra(POSITION_NAME, editNameInput.text.toString())
            replyIntent.putExtra(POSITION_NUMBER, editNumberInput.text.toString().toInt())
            setResult(Activity.RESULT_OK, replyIntent)
            finish()
        }
    }

    companion object {
        const val POSITION_ID = "POSITION_ID"
        const val POSITION_NAME = "POSITION_NAME"
        const val POSITION_NUMBER = "POSITION_NUMBER"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.wishlist_edit_position_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_back) {
            finish()
            return true
        }

        if (item.itemId == R.id.action_sign_out) {
            AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    finish()
                    startActivity(intent)
                }
        }

        return super.onOptionsItemSelected(item)
    }
}