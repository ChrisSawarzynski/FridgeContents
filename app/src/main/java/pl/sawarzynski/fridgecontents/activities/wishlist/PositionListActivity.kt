package pl.sawarzynski.fridgecontents.activities.wishlist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.auth.AuthUI
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.wishlist_position_list_activity.*
import pl.sawarzynski.fridgecontents.FridgeContentsApplication
import pl.sawarzynski.fridgecontents.R
import pl.sawarzynski.fridgecontents.activities.MainActivity
import pl.sawarzynski.fridgecontents.activities.contents.ProductListActivity
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPosition
import pl.sawarzynski.fridgecontents.model.wishlist.PositionViewModel
import pl.sawarzynski.fridgecontents.model.wishlist.PositionViewModelFactory
import java.time.LocalDateTime
import java.util.*


class PositionListActivity : AppCompatActivity() {
    private val positionViewModel: PositionViewModel by viewModels {
        PositionViewModelFactory(
            (application as FridgeContentsApplication).wishListPositionRepository,
            (application as FridgeContentsApplication).productRepository,
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wishlist_position_list_activity)

        val toolbar = appToolbar as Toolbar
        toolbar.title = getString(R.string.wishlist_title)
        setSupportActionBar(toolbar)

        val recyclerView = findViewById<RecyclerView>(R.id.positionListView)
        val adapter = PositionListAdapter(positionViewModel)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        positionViewModel.positions.observe(this, Observer { positions ->
            positions?.let { adapter.submitList(it) }
        })

        val fab = goToAddPosition as FloatingActionButton

        fab.setImageResource(android.R.drawable.ic_input_add)

        Handler().postDelayed(Runnable {
            fab.setOnClickListener {
                val intent = Intent(this, AddPositionActivity::class.java)
                startActivityForResult(intent, ADD_POSITION_REQUEST)
            }
        }, 200)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ADD_POSITION_REQUEST && resultCode == Activity.RESULT_OK) {
            var name: String = "-"
            var number: Int = 1

            data?.getStringExtra(AddPositionActivity.POSITION_NAME)?.let {
                name = it
            }

            data?.getSerializableExtra(AddPositionActivity.POSITION_NUMBER)?.let {
                number = it as Int
            }

            val position = WishListPosition(
                UUID.randomUUID().toString(),
                name,
                LocalDateTime.now(),
                number,
                false
            )

            positionViewModel.insert(position)
        }

        if (requestCode == EDIT_POSITION_REQUEST && resultCode == Activity.RESULT_OK) {
            var id: String = "-"
            var name: String = "-"
            var number: Int = 1

            data?.getStringExtra(EditPositionActivity.POSITION_ID)?.let {
                id = it
            }

            data?.getStringExtra(EditPositionActivity.POSITION_NAME)?.let {
                name = it
            }

            data?.getSerializableExtra(EditPositionActivity.POSITION_NUMBER)?.let {
                number = it as Int
            }

            val position = WishListPosition(
                id,
                name,
                LocalDateTime.now(),
                number,
                false
            )

            positionViewModel.update(position)
        }
    }

    companion object {
        const val ADD_POSITION_REQUEST = 1996
        const val EDIT_POSITION_REQUEST = 1966
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.wishlist_position_list_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_contents) {
            val intent = Intent(this, ProductListActivity::class.java)
            startActivity(intent)
            return true
        }

        if (item.itemId == R.id.action_send_wishlist) {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND

                val positionsBuilder = StringBuilder()
                positionsBuilder.append(
                    getString(
                        R.string.share_wishlist_welcome,
                        Firebase.auth.currentUser?.displayName
                    )
                )

                positionViewModel.positions.value?.forEach {
                    positionsBuilder.append("- ${it.name}:\t${it.number}\n")
                }

                putExtra(Intent.EXTRA_TEXT, positionsBuilder.toString())
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)

            startActivity(shareIntent)
        }

        if (item.itemId == R.id.action_clear_taken) {
            positionViewModel.positions.value?.filter { it.taken }?.forEach {
                positionViewModel.delete(it)
            }
        }

        if (item.itemId == R.id.action_sign_out) {
            AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    finish()
                    startActivity(intent)
                }
        }

        return super.onOptionsItemSelected(item)
    }
}