package pl.sawarzynski.fridgecontents.activities.contents

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.auth.AuthUI
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.contents_product_list_activity.*
import pl.sawarzynski.fridgecontents.FridgeContentsApplication
import pl.sawarzynski.fridgecontents.R
import pl.sawarzynski.fridgecontents.activities.MainActivity
import pl.sawarzynski.fridgecontents.activities.wishlist.PositionListActivity
import pl.sawarzynski.fridgecontents.data.contents.Product
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPosition
import pl.sawarzynski.fridgecontents.model.contents.ProductViewModel
import pl.sawarzynski.fridgecontents.model.contents.ProductViewModelFactory
import pl.sawarzynski.fridgecontents.model.wishlist.PositionViewModel
import pl.sawarzynski.fridgecontents.model.wishlist.PositionViewModelFactory
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*


class ProductListActivity : AppCompatActivity() {
    private val productViewModel: ProductViewModel by viewModels {
        ProductViewModelFactory((application as FridgeContentsApplication).productRepository)
    }

    private val positionViewModel: PositionViewModel by viewModels {
        PositionViewModelFactory(
            (application as FridgeContentsApplication).wishListPositionRepository,
            (application as FridgeContentsApplication).productRepository,
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contents_product_list_activity)

        val toolbar = appToolbar as Toolbar
        toolbar.title = getString(R.string.fridge_contents_title)
        setSupportActionBar(toolbar)

        val recyclerView = findViewById<RecyclerView>(R.id.productListView)
        val adapter = ProductListAdapter(productViewModel)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        productViewModel.products.observe(this, Observer { products ->
            products?.let { adapter.submitList(it) }
        })

        val fab = gotToAddProduct as FloatingActionButton

        productViewModel.selectedProducts.observe(this, Observer { selectedProducts ->
            if (selectedProducts.size > 0) {
                fab.setImageResource(android.R.drawable.ic_menu_delete)

                Handler().postDelayed(Runnable {
                    fab.setOnClickListener {
                        val builder = AlertDialog.Builder(this)

                        builder.apply {
                            setTitle(getString(R.string.add_to_wishlist_question))

                            setPositiveButton(getString(R.string.yes)) { _, _ ->
                                selectedProducts.forEach {
                                    val position = WishListPosition(
                                        UUID.randomUUID().toString(),
                                        it.name,
                                        LocalDateTime.now(),
                                        1,
                                        false
                                    )

                                    positionViewModel.insert(position)
                                }
                            }
                            setNegativeButton(getString(R.string.no)) { _, _ -> }
                            setOnDismissListener {
                                selectedProducts.forEach {
                                    productViewModel.delete(it)
                                }

                                selectedProducts.clear()
                                productViewModel.selectedProducts.value = selectedProducts
                            }
                        }
                        builder.show()
                    }
                }, 200)

            } else {
                fab.setImageResource(android.R.drawable.ic_input_add)

                Handler().postDelayed(Runnable {
                    fab.setOnClickListener {
                        val intent = Intent(this, AddProductActivity::class.java)
                        startActivityForResult(intent, ADD_PRODUCT_REQUEST)
                    }
                }, 200)

            }
        })
    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ADD_PRODUCT_REQUEST && resultCode == Activity.RESULT_OK) {
            var name: String = "-"
            var expires: LocalDate = LocalDate.now()

            data?.getStringExtra(AddProductActivity.PRODUCT_NAME)?.let {
                name = it
            }

            data?.getSerializableExtra(AddProductActivity.PRODUCT_EXPIRES)?.let {
                expires = it as LocalDate
            }

            val product = Product(
                UUID.randomUUID().toString(),
                name,
                LocalDateTime.now(),
                expires
            )

            productViewModel.insert(product)
        }

        if (requestCode == EDIT_PRODUCT_REQUEST && resultCode == Activity.RESULT_OK) {
            var id: String = "-"
            var name: String = "-"
            var expires: LocalDate = LocalDate.now()

            data?.getStringExtra(EditProductActivity.PRODUCT_ID)?.let {
                id = it
            }

            data?.getStringExtra(EditProductActivity.PRODUCT_NAME)?.let {
                name = it
            }

            data?.getSerializableExtra(EditProductActivity.PRODUCT_EXPIRES)?.let {
                expires = it as LocalDate
            }

            val product = Product(
                id,
                name,
                LocalDateTime.now(),
                expires
            )

            productViewModel.update(product)
        }
    }

    companion object {
        const val ADD_PRODUCT_REQUEST = 2137
        const val EDIT_PRODUCT_REQUEST = 333
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.contents_product_list_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_wishlist) {
            val intent = Intent(this, PositionListActivity::class.java)
            startActivity(intent)
            return true
        }

        if (item.itemId == R.id.action_sign_out) {
            AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    finish()
                    startActivity(intent)
                }
        }

        return super.onOptionsItemSelected(item)
    }
}