package pl.sawarzynski.fridgecontents.activities.contents

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pl.sawarzynski.fridgecontents.R
import pl.sawarzynski.fridgecontents.data.contents.Product
import pl.sawarzynski.fridgecontents.model.contents.ProductViewModel
import java.time.format.DateTimeFormatter


class ProductListAdapter(private val viewModel: ProductViewModel) : ListAdapter<Product, ProductListAdapter.ProductViewHolder>(
    ProductComparator()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {

        return ProductViewHolder.create(parent, viewModel)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current)
    }

    class ProductViewHolder(itemView: View, private val viewModel: ProductViewModel) : RecyclerView.ViewHolder(itemView) {
        private val selectProduct: CheckBox = itemView.findViewById(R.id.selectProduct)
        private val nameDisplay: TextView = itemView.findViewById(R.id.nameDisplay)
        private val expiresDisplay: TextView = itemView.findViewById(R.id.expiresDisplay)

        fun bind(product: Product) {
            nameDisplay.text = product.name
            expiresDisplay.text = product.expires.format(DateTimeFormatter.ISO_LOCAL_DATE)

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, EditProductActivity::class.java)
                intent.putExtra(PRODUCT_ID, product.id)
                intent.putExtra(PRODUCT_NAME, product.name)
                intent.putExtra(PRODUCT_EXPIRES, product.expires)
                val context = (itemView.context as Activity)
                context.startActivityForResult(intent, ProductListActivity.EDIT_PRODUCT_REQUEST)
            }

            val initialSelectedProducts = viewModel.selectedProducts.value

            selectProduct.isChecked = initialSelectedProducts != null && initialSelectedProducts.contains(product)

            selectProduct.setOnCheckedChangeListener { _, isChecked ->
                val selectedProducts = viewModel.selectedProducts.value

                if (isChecked) {
                    selectedProducts?.add(product)
                } else {
                    selectedProducts?.remove(product)
                }

                viewModel.selectedProducts.value = selectedProducts
            }
        }

        companion object {
            fun create(parent: ViewGroup, viewModel: ProductViewModel): ProductViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.contents_product_list_item, parent, false)
                return ProductViewHolder(view, viewModel)
            }

            const val PRODUCT_ID = "PRODUCT_ID"
            const val PRODUCT_NAME = "PRODUCT_NAME"
            const val PRODUCT_EXPIRES = "PRODUCT_EXPIRES"
        }
    }

    class ProductComparator : DiffUtil.ItemCallback<Product>() {
        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.id === newItem.id
        }

        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.name == newItem.name && oldItem.expires == newItem.expires
        }
    }
}

