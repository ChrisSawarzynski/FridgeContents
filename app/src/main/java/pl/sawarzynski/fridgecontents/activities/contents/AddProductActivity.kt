package pl.sawarzynski.fridgecontents.activities.contents

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.firebase.ui.auth.AuthUI
import kotlinx.android.synthetic.main.contents_add_product_activity.*
import pl.sawarzynski.fridgecontents.R
import pl.sawarzynski.fridgecontents.activities.MainActivity
import java.time.LocalDate
import java.time.format.DateTimeFormatter


class AddProductActivity : AppCompatActivity() {
    private var expiresDate: LocalDate = LocalDate.now()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contents_add_product_activity)

        val toolbar = appToolbar as Toolbar
        toolbar.title = getString(R.string.add_to_fridge_title)
        setSupportActionBar(toolbar)

        editExpiresInput.setText(expiresDate.format(DateTimeFormatter.ISO_LOCAL_DATE))

        val dateSetListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            this.expiresDate = LocalDate.of(year, month, dayOfMonth)
            editExpiresInput.setText(expiresDate.format(DateTimeFormatter.ISO_LOCAL_DATE))
        }

        editExpiresInput.setOnClickListener {
            DatePickerDialog(
                this,
                dateSetListener,
                expiresDate.year,
                expiresDate.monthValue,
                expiresDate.dayOfMonth
            ).show()
        }

        addProductButton.setOnClickListener {
            val replyIntent = Intent()
            replyIntent.putExtra(PRODUCT_NAME, editNameInput.text.toString())
            replyIntent.putExtra(PRODUCT_EXPIRES, expiresDate)
            setResult(Activity.RESULT_OK, replyIntent)
            finish()
        }
    }

    companion object {
        const val PRODUCT_NAME = "PRODUCT_NAME"
        const val PRODUCT_EXPIRES = "PRODUCT_EXPIRES"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.contents_add_product_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_back) {
            finish()
            return true
        }

        if (item.itemId == R.id.action_sign_out) {
            AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    finish()
                    startActivity(intent)
                }
        }

        return super.onOptionsItemSelected(item)
    }
}
