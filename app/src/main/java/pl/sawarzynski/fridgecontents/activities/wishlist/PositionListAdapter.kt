package pl.sawarzynski.fridgecontents.activities.wishlist

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pl.sawarzynski.fridgecontents.R
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPosition
import pl.sawarzynski.fridgecontents.model.wishlist.PositionViewModel

class PositionListAdapter(private val viewModel: PositionViewModel) : ListAdapter<WishListPosition, PositionListAdapter.PositionViewHolder>(
    PositionComparator()
)  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PositionViewHolder {
        return PositionViewHolder.create(parent, viewModel)
    }

    override fun onBindViewHolder(holder: PositionViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current)
    }

    class PositionViewHolder(itemView: View, private val viewModel: PositionViewModel) : RecyclerView.ViewHolder(itemView) {
        private val alreadyHaveButton: CheckBox = itemView.findViewById(R.id.alreadyHaveButton)
        private val nameDisplay: TextView = itemView.findViewById(R.id.nameDisplay)
        private val numberDisplay: TextView = itemView.findViewById(R.id.numberDisplay)

        fun bind(position: WishListPosition) {
            nameDisplay.text = position.name
            numberDisplay.text = position.number.toString()

            alreadyHaveButton.isChecked = position.taken

            alreadyHaveButton.setOnCheckedChangeListener { button, isChecked ->
                if (!button.isPressed) {
                    return@setOnCheckedChangeListener
                }

                position.taken = isChecked

                viewModel.update(position)
            }

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, EditPositionActivity::class.java)
                intent.putExtra(POSITION_ID, position.id)
                intent.putExtra(POSITION_NAME, position.name)
                intent.putExtra(POSITION_NUMBER, position.number)
                val context = (itemView.context as Activity)
                context.startActivityForResult(intent, PositionListActivity.EDIT_POSITION_REQUEST)
            }
        }

        companion object {
            fun create(parent: ViewGroup, viewModel: PositionViewModel): PositionViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.wishlist_position_list_item, parent, false)

                return PositionViewHolder(view, viewModel)
            }

            const val POSITION_ID = "POSITION_ID"
            const val POSITION_NAME = "POSITION_NAME"
            const val POSITION_NUMBER = "POSITION_NUMBER"
        }
    }

    class PositionComparator : DiffUtil.ItemCallback<WishListPosition>() {
        override fun areItemsTheSame(oldItem: WishListPosition, newItem: WishListPosition): Boolean {
            return oldItem.id === newItem.id
        }

        override fun areContentsTheSame(oldItem: WishListPosition, newItem: WishListPosition): Boolean {
            return oldItem.name == newItem.name
                    && oldItem.number == newItem.number
                    && oldItem.taken == newItem.taken
                    && oldItem.id == newItem.id
        }
    }
}