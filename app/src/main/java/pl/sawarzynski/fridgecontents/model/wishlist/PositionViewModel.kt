package pl.sawarzynski.fridgecontents.model.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import pl.sawarzynski.fridgecontents.data.contents.ProductRepository
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPosition
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPositionRepository

class PositionViewModel(
    private val wishListPositionRepository: WishListPositionRepository,
    private val productRepository: ProductRepository
) : ViewModel() {

    val positions: LiveData<List<WishListPosition>> = wishListPositionRepository.positions.asLiveData()

    fun insert(wishListPosition: WishListPosition) = viewModelScope.launch {
        wishListPositionRepository.insert(wishListPosition)
    }

    fun update(wishListPosition: WishListPosition) = viewModelScope.launch {
        wishListPositionRepository.update(wishListPosition)
    }

    fun delete(wishListPosition: WishListPosition) = viewModelScope.launch {
        wishListPositionRepository.delete(wishListPosition)
    }
}
