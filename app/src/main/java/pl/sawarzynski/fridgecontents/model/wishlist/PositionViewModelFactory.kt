package pl.sawarzynski.fridgecontents.model.wishlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import pl.sawarzynski.fridgecontents.data.contents.ProductRepository
import pl.sawarzynski.fridgecontents.data.wishlist.WishListPositionRepository

class PositionViewModelFactory(
    private val wishListPositionRepository: WishListPositionRepository,
    private val productRepository: ProductRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PositionViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PositionViewModel(wishListPositionRepository, productRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}