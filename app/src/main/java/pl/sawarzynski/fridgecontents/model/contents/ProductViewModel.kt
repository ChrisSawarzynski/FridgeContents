package pl.sawarzynski.fridgecontents.model.contents

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import pl.sawarzynski.fridgecontents.data.contents.Product
import pl.sawarzynski.fridgecontents.data.contents.ProductRepository

class ProductViewModel(private val productRepository: ProductRepository) : ViewModel() {

    val products: LiveData<List<Product>> = productRepository.products.asLiveData()

    val selectedProducts: MutableLiveData<MutableList<Product>> by lazy {
        MutableLiveData<MutableList<Product>>(mutableListOf())
    }


    fun insert(product: Product) = viewModelScope.launch {
        productRepository.insert(product)
    }

    fun update(product: Product) = viewModelScope.launch {
        productRepository.update(product)
    }

    fun delete(product: Product) = viewModelScope.launch {
        productRepository.delete(product)
    }
}
